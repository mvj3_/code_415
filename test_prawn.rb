# coding: utf-8
require "rubygems"
require 'prawn' 
Prawn::Document.generate('hello.pdf') do |pdf| 
  pdf.font "#{Prawn::BASEDIR}/data/fonts/gkai00mp.ttf"  
  pdf.text("Hello Prawn!") 
  pdf.text("我是ice。。。。。 from www.eoe.cn") 
  pdf.image "#{Prawn::BASEDIR}/data/images/rails.png"
  pdf.text("this is a link <link href='http://www.eoe.cn'>eoe.cn </link>", :inline_format => true)
end